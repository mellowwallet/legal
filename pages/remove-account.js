export default function Privacy() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo";
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com";

    return (
        <>
            <div className="terms">
                <h2>ACCOUNT & PERSONAL INFORMATION DELETION POLICY</h2>
                <p>
                    At <strong>{client}</strong>, we value the privacy and security of our users' data. The personal and
                    account information we store is <strong>never shared with third parties</strong> under any
                    circumstances.
                </p>
                <p>
                    The only data we store includes your <strong>alias, public addresses, profile picture, and encrypted chats</strong>. 
                    These will be permanently removed if you request account deletion.
                </p>
                <p>
                    If you wish to request the deletion of your account and associated data, you can do so directly
                    within our app by following these steps:
                </p>
                <ul>
                    <li>
                        Go to <strong>Settings</strong>.
                    </li>
                    <li>
                        Navigate to the <strong>Accounts and profiles</strong> section.
                    </li>
                    <li>
                        Select <strong>Remove account</strong>.
                    </li>
                </ul>
                <p>
                    This process will <strong>permanently delete your account</strong> along with all associated data.
                </p>
                <p>
                    If you prefer to contact us directly to request data deletion or have additional questions, please
                    contact us at <strong>{support}</strong>.
                </p>
                <p>
                    <strong>{client}</strong> complies with all applicable regulations regarding data retention and
                    deletion, ensuring that your data is handled securely and ethically.
                </p>
                <p>
                    Please note that <strong>deleting your account will never result in the loss of your funds</strong>,
                    as your funds are securely stored using your <strong>"seed phrase"</strong>. 
                    This seed phrase allows you to access your funds from any cryptocurrency wallet.
                </p>
            </div>
        </>
    );
}
