

export default function Privacy() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo"
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com"

    return (
        <>
            <div className="terms">
                <h2>PRIVACY POLICY</h2>
                <p></p>
                <p></p>
                <p>This privacy notice for <span>{client}</span> ( "Company ",  "we " or  "our ") describes how and why we may collect, store, use and/or share ( "process ") your information when you use our services ( "Services "), such as when you:</p>
                <ul>
                    <li>
                        <p>Download and use our mobile application, or any other application of ours that links to this privacy notice.</p>
                    </li>
                    <li>
                        <p>Engage with us in other related ways, including sales, marketing, or events.</p>
                    </li>
                </ul>
                <p>Questions or concerns? Reading this privacy notice will help you understand your privacy rights and choices. If you do not agree with our policies and practices, please do not use our Services. If you still have any questions or concerns, please contact us at {support}.</p>
                <p></p>
                <p></p>
                <h4>SUMMARY OF KEY POINTS</h4>
                <p></p>
                <p>This summary provides key points from our privacy notice, but you can find more detail on any of these topics by clicking on the link that follows each key point or by using our table of contents below to find the section you are interested in. searching.  </p>
                <p></p>
                <p><strong>Do we process any sensitive personal information?</strong> We do not process sensitive personal information.</p>
                <p></p>
                <p><strong>Do you receive any information from third parties?</strong> We do not receive personal information from public databases, marketing partners, social media platforms and other outside sources.</p>
                <p></p>
                <p><strong>How do you process my information?</strong> We process your information to provide, improve, and administer our Services, to communicate with you, for security and fraud prevention, and to comply with the law. We may also process your information for other purposes with your consent. We process your information only where we have a valid legal reason to do so. </p>
                <p></p>
                <p><strong>In what situations and with what parties do we share personal information?</strong> We may share information in specific situations and with specific third parties, but never personal information that could compromise you. </p>
                <p></p>
                <p><strong>What are your rights?</strong> Depending on where you are geographically located, applicable privacy law may mean that you have certain rights with respect to your personal information. </p>
                <p></p>
                <p><strong>How do I exercise my rights?</strong> The easiest way to exercise your rights is by contacting us. We will consider and act on any request in accordance with applicable data protection laws.</p>
                <p></p>
                <p></p>
                <h4>WHAT INFORMATION DO WE COLLECT?</h4>
                <p></p>
                <p>Personal information you disclose to us</p>
                <p></p>
                <p>In short: collect the personal information you provide to us.</p>
                <p></p>
                <p>We collect personal information that you voluntarily provide to us when you register for the Services, express an interest in learning about us or our products and Services, engage in activities on the Services, or otherwise communicate with us.</p>
                <p></p>
                <p>Sensitive information. We do not process sensitive information.</p>
                <p></p>
                <p>All personal information you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.</p>
                <p></p>
                <p>Automatically Collected Information</p>
                <p></p>
                <p>In Short: Some information, such as your Internet Protocol (IP) address and/or browser and device characteristics, is automatically collected when you visit our Services.</p>
                <p></p>
                <p>We automatically collect certain information when you visit, use or browse the Services. This information does not reveal your specific identity (such as your name or contact information), but may include device and usage information such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location. , information about how and when you use our Services and other technical information. This information is primarily needed to maintain the security and operation of our Services, and for our internal reporting and analytics purposes.</p>
                <p></p>
                <p>Like many companies, we also collect information through cookies and similar technologies.</p>
                <p></p>
                <p>HOW DO WE PROCESS YOUR INFORMATION?</p>
                <p></p>
                <p>In Short: We process your information to provide, improve, and administer our Services, to communicate with you, for security and fraud prevention, and to comply with the law. We may also process your information for other purposes with your consent.</p>
                <p></p>
                <p>We process your personal information for a variety of reasons, depending on how you interact with our Services, including:</p>
                <p></p>
                <p>WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?</p>
                <p></p>
                <p>In Short: We may share information in specific situations described in this section and/or with the following third parties.</p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <h4>DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</h4>
                <p></p>
                <p>In short: We may use cookies and other tracking technologies to collect and store your information.</p>
                <p></p>
                <p>We may use cookies and similar tracking technologies (such as web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Notice.</p>
                <p></p>
                <p>IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY?</p>
                <p></p>
                <p>In short: We may transfer, store and process your information in countries other than your own, but always within our database.</p>
                <p></p>
                <p>Our servers are located at. If you access our Services from abroad, please note that your information may be transferred to, stored and processed by us at our facilities and by those third parties with whom we may share your personal information (see  "WHEN AND WITH WHOM WE SHARE YOUR PERSONAL INFORMATION ? " above), in and other countries.</p>
                <p></p>
                <p>If you reside in the European Economic Area (EEA) or the United Kingdom (UK), these countries may not necessarily have data protection or other similar laws as comprehensive as those in your country. However, we will take all necessary steps to protect your personal information in accordance with this privacy notice and applicable law.</p>
                <p></p>
                <h4>HOW LONG DO WE KEEP YOUR INFORMATION?</h4>
                <p></p>
                <p>In Short: We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice, unless otherwise required by law.</p>
                <p></p>
                <p>We will only retain your personal information for as long as necessary for the purposes set out in this privacy notice, unless a longer retention period is required or permitted by law (such as tax, accounting, or other legal requirements).</p>
                <p></p>
                <p>Where we do not have an ongoing legitimate business need to process your personal information, we will delete or anonymize such information or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will do so securely. . store your personal information and isolate it from any further processing until it is possible to delete it.</p>
                <p></p>
                <h4>DO WE COLLECT INFORMATION FROM MINORS?</h4>
                <p></p>
                <p>In short: We do not knowingly collect data from children under the age of 18 or market it.</p>
                <p></p>
                <p>We do not knowingly solicit data from or market to children under the age of 18. By using the Services, you represent that you are at least 18 years of age or the parent or guardian of such dependent minor and consent to the use of the Services by such dependent minor. If we learn that personal information has been collected from users under the age of 18, we will deactivate the account and take reasonable steps to promptly remove such information from our records. If you become aware of any data we may have collected from children under the age of 18, please contact us.</p>
                <p></p>
                <h4>WHAT ARE YOUR PRIVACY RIGHTS?</h4>
                <p></p>
                <p>In short:  you can review, change or cancel your account at any time.</p>
                <p> </p>
                <p>If you are located in the EEA or the UK and believe that we are processing your personal information unlawfully, you also have the right to lodge a complaint with your local data protection supervisory authority. You can find their contact details here: <a href="https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm">https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm</a>.</p>
                <p></p>
                <p>If you are located in Switzerland, the contact details of the data protection authorities are available here: <a href="https://www.edoeb.admin.ch/edoeb/en/home.html">https://www.edoeb.admin.ch/edoeb/en/home.html</a>.</p>
                <p></p>
                <p>Withdraw your consent: If we rely on your consent to process your personal information, which may be express or implied under applicable law, you have the right to withdraw your consent at any time. You can withdraw your consent at any time by contacting us.</p>
                <p></p>
                <p>Please note, however, that this will not affect the lawfulness of processing prior to your withdrawal, nor, where permitted by applicable law, will it affect processing of your personal information carried out on the basis of legal processing grounds other than consent.</p>
                <p></p>
                <p>Account Information</p>
                <p></p>
                <p>If at any time you wish to review or change your account information or cancel your account, you may. If you request to cancel your account, we will deactivate or delete your account and information from our active databases. However, we may retain certain information in our files to prevent fraud, troubleshoot problems, assist with any investigation, enforce our legal terms, and/or comply with applicable legal requirements.</p>
                <p></p>
                <h4>CONTROLS FOR DO NOT TRACK FEATURES</h4>
                <p></p>
                <p>Most web browsers and some mobile operating systems and mobile applications include a Do Not Track ( "DNT ") feature or setting that you can enable to signal your privacy preference not to have data about your activities. online browsing monitored and collected. At this stage, no uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will tell you about that practice in a revised version of this privacy notice.</p>
                <p></p>
                <h4>DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</h4>
                <p></p>
                <p>In Short: Yes, if you are a California resident, you are granted specific rights regarding access to your personal information.</p>
                <p></p>
                <p>California Civil Code Section 1798.83, also known as the  "Shine The Light " law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information ( if applicable) that we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with whom we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please send us your request in writing using the contact information provided below.</p>
                <p></p>
                <p>If you are under the age of 18, reside in California, and have a registered account on the Services, you have the right to request removal of unwanted data that you publicly post on the Services. To request deletion of such data, please contact us using the contact information provided below and include the email address associated with your account and a statement that you reside in California. We will ensure that data is not publicly displayed on the Services, but please note that data may not be completely or completely removed from all of our systems (eg, backups, etc.).</p>
                <p></p>
                <h4>DO WE MAKE UPDATES TO THIS NOTICE?</h4>
                <p></p>
                <p>In Short: Yes, we will update this notice as necessary to comply with applicable law.</p>
                <p></p>
                <p>We may update this privacy notice from time to time. The updated version will be indicated by an updated  "Revised " date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes or by sending you a notice directly. We encourage you to review this privacy notice frequently to be informed of how we protect your information.</p>
                <p></p>
                <h4>HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</h4>
                <p></p>
                <p>If you have questions or comments about this notice, you can email us at {support}.</p>
                <p></p>
                <p>HOW CAN YOU REVIEW, UPDATE OR DELETE THE DATA WE COLLECT FROM YOU?</p>
                <p></p>
                <p>Under the applicable laws of your country, you may have the right to request access to the personal information we collect from you, change that information or delete it in some circumstances. To request to review, update, or delete your personal information, please email us at {support}.</p>
            </div>

        </>

    )
}
