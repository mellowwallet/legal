export default function Privacy() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo";
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com";

    return (
        <>
            <div className="terms">
                <h2>SUPPORT</h2>
                <p></p>
                <p></p>
                <p>
                    Welcome to the support center of <span>{client}</span>. Here you will find answers to questions FAQs
                    and solutions to common problems so you can get the most out of your user experience. use of our
                    application. Our support team is here to help you at all times, so If you can't find the answer you
                    need, don't hesitate to contact us.
                </p>
                <p></p>
                <p></p>

                <p>
                    <strong>
                        What does it mean that the cryptocurrency wallet is decentralized and that they do not have
                        access to customer funds?
                    </strong>
                </p>
                <p>
                    A decentralized cryptocurrency wallet means that funds are not stored in a centralized server, but
                    are stored on the blockchain network. Not having access to funds customer, privacy and security are
                    significantly improved, as customers have complete control over your own transactions.
                </p>

                <p>
                    <strong>How does the wallet improve the experience of using cryptocurrency?</strong>
                </p>
                <p>
                    The wallet aims to improve the experience of using cryptocurrencies by offering a User-friendly
                    interface and intuitive functionality. Also, chat and alias integration (and avatar) allow users to
                    easily identify each other, making it easy to carrying out transactions.
                </p>

                <p>
                    <strong>How does the wallet ensure transparency in transactions?</strong>
                </p>
                <p>
                    Transparency in transactions is achieved through blockchain technology. Each transaction is written
                    to the blockchain, which means it is permanently recorded and can be publicly verify. As a result,
                    transactions are fully transparent and secure.
                </p>

                <p>
                    <strong>
                        How does the chat integrated in the wallet help to simplify the process of sending
                        cryptocurrencies?
                    </strong>
                </p>
                <p>
                    The integrated chat in the wallet allows users to send cryptocurrencies more easily and avoid errors
                    when copying another user's address. This is because chat allows users to users send cryptocurrency
                    directly to the recipient's address without having to copy it manually.
                </p>

                <p>
                    <strong>How does the wallet improve device security?</strong>
                </p>
                <p>
                    The wallet has an access restriction through biometric data, which means that only the owner of the
                    device can access the wallet. This significantly improves the security of the wallet, since the user
                    data can only be accessed by the device owner.
                </p>

                <p>
                    <strong>How can the funds be recovered if the device containing the wallet is lost?</strong>
                </p>
                <p>
                    To recover the funds in case of losing the device, it is necessary to have a copy of security of the
                    recovery phrase or seed phrase. This is a random word phrase that is generated when creating the
                    wallet, and that allow to recover the funds in case of loss of the device.
                </p>

                <p>
                    <strong>Does the wallet support multiple cryptocurrencies or just one?</strong>
                </p>
                <p>
                    The wallet can support various cryptocurrencies, depending on the configuration of the wallet. In
                    Generally, decentralized wallets can support multiple cryptocurrencies, making them more versatile
                    and useful for users.
                </p>

                <p>
                    <strong>Does the wallet charge any fees for the use of the wallet?</strong>
                </p>
                <p>The wallet is free and does not charge any fees for the use of the wallet.</p>

                <p>
                    <strong>Does the app charge any additional costs for cryptocurrency transactions?</strong>
                </p>
                <p>
                    No, the app does not charge any additional fees for cryptocurrency transactions. The Users will only
                    pay the service cost of each blockchain corresponding to the transaction, and the application will
                    not charge anything additional. This allows users to save on transaction costs and make better use
                    of your cryptocurrencies.
                </p>

                <h4>We leave you some tips for when you start working with cryptocurrencies</h4>
                <ul>
                    <li>
                        {" "}
                        <p>
                            Do Thorough Research: Before investing in any cryptocurrency, make sure to investigate in
                            depth about cryptocurrency, its development team, its technology underlying, its objectives
                            and its potential.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Understand the risks: Cryptocurrencies are a volatile asset, and prices can fluctuate
                            drastically. Understand the risks of investing in cryptocurrencies before doing so.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Invest only what you can afford to lose: Cryptocurrencies are a high value asset. risk, so
                            you should only invest money that you can afford to lose without affecting your quality of
                            life.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Store your cryptocurrencies in a secure wallet: Store your cryptocurrencies in a secure
                            wallet that allows you to have full control over your private keys.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Check transactions carefully: Check transactions carefully before to confirm them to avoid
                            sending cryptocurrencies to the wrong address.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Keep your private keys safe: Your private keys are the only way to access your
                            cryptocurrencies, so it is important to keep them safe and secure.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Diversify your investment: It is important to diversify your investment in different
                            cryptocurrencies to minimize the risks of a single investment.
                        </p>
                    </li>
                    <li>
                        {" "}
                        <p>
                            Do not get carried away by emotion: Do not get carried away by emotion and the news of the
                            market when making investment decisions. Make informed and rational decisions.
                        </p>
                    </li>
                </ul>

                <p></p>
                <p></p>
                <h4>
                    Thank you for using our cryptocurrency wallet. If you have any questions or problems, don't feel
                    free to contact us. We are here to help you in everything you need. Can contact us through our
                    support email (<a href={`mailto:${support}`}>{support}</a>) We are looking forward to helping you!
                </h4>

                <a className="button" href={`mailto:${support}`}>
                    Contact Us
                </a>
            </div>
        </>
    );
}
