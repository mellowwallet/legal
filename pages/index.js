import { NavLink } from "react-bootstrap";

export default function Home() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo";
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com";

    return (
        <>
            <div className="terms">
                <h1>
                    <span>{client}</span> Legal
                </h1>
                <div className="title">
                    <NavLink href="terms">Terms and Conditions</NavLink>
                </div>
                <div className="title" style={{ marginTop: 5 }}>
                    <NavLink href="uela">End User License Agreement (UELA)</NavLink>
                </div>

                <div className="title" style={{ marginTop: 5 }}>
                    <NavLink href="privacy">Privacy Policy</NavLink>
                </div>
                <div className="title" style={{ marginTop: 5 }}>
                    <NavLink href="account">Account & Personal Information</NavLink>
                </div>

                <div className="title" style={{ marginTop: 5 }}>
                    <NavLink href="remove-account">Account & Personal Information Deletion Policy</NavLink>
                </div>
                
                
                <div className="title" style={{ marginTop: 5 }}>
                    <NavLink href="support">Support</NavLink>
                </div>
            </div>
        </>
    );
}
