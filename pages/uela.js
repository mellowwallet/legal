export default function Privacy() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo";
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com";

    return (
        <>
            <div className="terms">
                <h2>End User License Agreement (EULA)</h2>
                <p>
                    Please read this End User License Agreement ("EULA") carefully before using the {client} application
                    (the "Application"). By using the Application, you agree to be bound by the terms and conditions of
                    this EULA. If you do not agree to these terms and conditions, please do not use the Application.
                </p>

                <h4>Limited License:</h4>
                <p>
                    You are granted a limited, non-exclusive, non-transferable, revocable license to use the Application
                    on your mobile device for personal, non-commercial purposes. You are not permitted to copy, modify,
                    distribute, sell, rent, sublicense or transfer the Application or any part of it.
                </p>

                <h4>Use restrictions:</h4>
                <p>
                    The Application is intended for use by persons over the age of 17. You agree not to use the
                    Application for any illegal or unauthorized purpose. You are not permitted to use the Application to
                    send or receive any objectionable or abusive content, including, but not limited to, messages
                    containing offensive language, harassment, defamation, threats, discrimination, sexually explicit or
                    violent content. You are solely responsible for the content you submit through the Application.
                </p>

                <h4>Security:</h4>
                <p>
                    You are responsible for maintaining the security of your Application account and seed phrase. You
                    are not allowed to share your account or seed phrase with third parties or allow third parties to
                    access your account. You agree to notify us immediately of any unauthorized use of your account or
                    any other breach of security related to the Application.
                </p>

                <h4>Warranty Disclaimer:</h4>
                <p>
                    The Application is provided "as is" without warranty of any kind, either express or implied. We do
                    not warrant that the Application will meet your needs or that it will be uninterrupted, error-free,
                    or secure. You agree that you use the Application at your own risk.
                </p>

                <h4>Limitation of Liability:</h4>
                <p>
                    In no event will we be liable for any indirect, special, incidental, punitive, or consequential
                    damages, including, but not limited to, lost profits, lost data, or business interruption, even if
                    we have been notified of the possibility of said damages. Our total liability under this EULA will
                    be limited to the total amount of money you have paid for the Application, if any.
                </p>

                <h4>Intellectual property:</h4>
                <p>
                    The Application and all related content, including, but not limited to, graphics, images, logos,
                    designs, trademarks, trade names, software code, and any other intellectual property related to the
                    Application, are the exclusive property of the owners of the Application. Application or its
                    licensors. You do not have the right to use any intellectual property related to the Application
                    without the prior written consent of the owners of the Application.
                </p>

                <h4>Termination:</h4>
                <p>
                    If you wish to terminate this EULA, you must uninstall the Application from your mobile device. All
                    provisions of this EULA that by their nature should survive termination shall survive termination,
                    including, but not limited to, the intellectual property, limitation of liability, warranty
                    disclaimer, and usage restrictions provisions.
                </p>

                <h4>Modifications:</h4>
                <p>
                    We may update or modify this EULA at any time and for any reason, without notice. Any changes or
                    modifications will be effective immediately upon posting on the Application. Your continued use of
                    the Application after any changes or modifications will constitute your acceptance of the updated
                    terms and conditions.
                </p>

                <h4>Applicable Law:</h4>
                <p>
                    This EULA shall be governed by and construed in accordance with the laws of the Application owners'
                    home country, without regard to its conflict of law provisions. Any dispute arising out of this EULA
                    shall be resolved in the competent courts of the country of origin of the owners of the Application.
                </p>

                <h4>Entire Agreement:</h4>
                <p>
                    This EULA constitutes the entire agreement between you and the owners of the Application regarding
                    the use of the Application and supersedes all prior or contemporaneous agreements and
                    understandings, whether written or oral, relating to the Application.
                </p>
                <p></p>
                <p></p>
                <p>
                    By clicking the "Accept" button or by using the Application, you acknowledge that you have read and
                    understood this EULA, and agree to be legally bound by its terms and conditions. If you do not agree
                    to these terms and conditions, please do not use the Application.
                </p>
            </div>
        </>
    );
}
