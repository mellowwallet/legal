

export default function Privacy() {
    const client = process.env.NEXT_PUBLIC_CLIENT || "beexo"
    const support = process.env.NEXT_PUBLIC_SUPPORT || "support@beexo.com"

    return (
        <>
            <div className="terms">
                <h2>ACCOUNT & PERSONAL INFORMATION</h2>
                <p></p>
                <p></p>
                <p>Many privacy laws give people rights over their personal data. These laws include the General Data Protection Regulation or GDPR. Your account is based on a Seed Phrase and has a Web3 foundation, so you own your information, we do not collect or track your movements or behavior within the application; We never have access to your seed phrase or your funds used in the wallet section of our application, nor can we access the messages you send in our chat, since they are end-to-end encrypted.</p>
                

                <h4>Can I check the data you have collected about my account?</h4>
                <p>Yes, the data we store are those that you have provided us, such as name, surname, email, avatar, etc. If you want us to notify you, you can write to our support through this email <a>support@beexo.com</a></p>
                
                
                <h4>Can I delete my account and personal information?</h4>
                <p>You can <span>delete</span> your personal data and your account, also unlink it from your email if you have used oAuth to enter. To do this, follow these steps:</p>
                <ul>
                    <li>
                        <p>Enter the App or the webApp</p>
                    </li>
                    <li>
                        <p>On the menu <span>SideBar</span> tap on the option <span>Setting</span> and then in <span>Accounts and profiles</span></p>
                    </li>

                    <li>
                        <p>There you will find the option called <span>Delete Account</span>, where we will ask you to confirm your decision to delete the account, once you touch the confirm button we will delete your data and unlink your account from your email.</p>
                    </li>
                </ul>
                <p>Keep in mind that if you have funds in the wallet, NFTs, POAPs or any other Web3.0 element you can recover it if you write down your seed phrase and re-enter it in our app or any decentralized wallet.</p>
                
            </div>

        </>

    )
}
